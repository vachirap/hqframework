import React, { Component } from 'react'
import {Switch, Route, BrowserRouter} from 'react-router-dom'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import ADrawer from './ADrawer'
//import routes from './ASwitch'
import MainLayout from './MainLayout';

const Inbox = props => <div>Inbox</div>
const Starred = props => <div>Starred</div>

const routes = <Switch>
  <Route path="/inbox" component={Inbox}/>
  <Route path="/starred" component={Starred}/>
</Switch>

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <MainLayout
          title = 'Hi-Q Framework Demo'
          drawer = {<ADrawer/>}
          routes = {routes}
        />
      </BrowserRouter>
    )
  }
}

export default App;
