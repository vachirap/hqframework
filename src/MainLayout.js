import React, { Component } from 'react'
import { Toolbar, AppBar, Typography, withStyles } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 430,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
  },
  toolbar: theme.mixins.toolbar,
})

const MainLayout = props =>
  <div className={props.classes.root}>
    <AppBar position="absolute" color="default"
      className={props.classes.appBar}
    >
      <Toolbar>
        <Typography variant="title" color="inherit">
          {props.title}
        </Typography>
      </Toolbar>
    </AppBar>
    {props.drawer}
    <main className={props.classes.content}>
      <div className={props.classes.toolbar} />
      {props.routes}
    </main>
  </div>

  export default withStyles(styles)(MainLayout)