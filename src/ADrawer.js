import React from 'react'
import {Link} from 'react-router-dom'
import classNames from 'classnames'
import Drawer from '@material-ui/core/Drawer'
import Divider from '@material-ui/core/Divider'
import {withStyles} from '@material-ui/core/styles'
import { Button } from '@material-ui/core';

const drawerWidth = 240

const styles = theme => ({
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar
})

const ADrawer = props =>
  <Drawer variant="permanent"
    open={true}
    classes={{
      paper: props.classes.drawerPaper,
    }}
  >
    <div className={props.classes.toolbar}/>
    <Divider/>
    <Button variant="contained" color="default">
      <Link to="/inbox">Inbox</Link>
    </Button>
    <Button variant="contained" color="default">
      <Link to="/starred">Starred</Link>
    </Button>
    <Button variant="contained" color="default">All mails</Button>
    <Button variant="contained" color="default">Trash</Button>
    <Divider/>
  </Drawer>

export default withStyles(styles)(ADrawer)