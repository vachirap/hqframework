import React from 'react'
import { TextField, withStyles } from '@material-ui/core'

const Login = props =>
  <form>
    <TextField
      id = "username"
      label = "Username"
      className = {props.classes.TextField}
    />
    <TextField
      id = "password"
      label = "Password"
      type = "password"
      className = {props.classes.TextField}
    />
</form>

export default withStyles({})(Login)